<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\Objectbrick;

use Exception;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\Objectbrick\Data\TestBrick;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class ObjectbrickTest extends TestCase
{
    private TestObject $testObject;
    private TestObject $oldRelation;
    private TestObject $newRelation;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/Objectbrick/Objectbrick');
        $this->oldRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->oldRelation->save();
        $this->newRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->newRelation->save();
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickIfFieldAdded()
    {
        $this->testObject->save();

        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickIfFieldDeleted()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);
        $this->testObject->save();

        $this->testObject->getObjectbricks()->setTestBrick(null);
        $this->testObject->save();


        $this->assertEquals(['TestObject.TestBrick'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickInputIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setInput('old');
        $this->testObject->save();
        $objectbrick->setInput('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.input'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickTextareaIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setTextarea('old');
        $this->testObject->save();
        $objectbrick->setTextarea('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.textArea'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickCheckboxIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setCheckbox(true);
        $this->testObject->save();
        $objectbrick->setCheckbox(false);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.checkbox'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickNumberIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setNumber(1);
        $this->testObject->save();
        $objectbrick->setNumber(2);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.number'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickQuantityValueIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setQuantityValue(new QuantityValue(20));
        $this->testObject->save();
        $objectbrick->setQuantityValue(new QuantityValue(30));
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.quantityValue'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickSelectIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setSelect('old');
        $this->testObject->save();
        $objectbrick->setSelect('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.select'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickMultiselectIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setMultiselect(['old']);
        $this->testObject->save();
        $objectbrick->setMultiselect(['new']);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.multiSelect'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickManyToOneRelationIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);


        $objectbrick->setManyToOneRelation($this->oldRelation);
        $this->testObject->save();
        $objectbrick->setManyToOneRelation($this->newRelation);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.manyToOneRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickManyToManyRelationIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);


        $objectbrick->setManyToManyRelation([$this->oldRelation]);
        $this->testObject->save();
        $objectbrick->setManyToManyRelation([$this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.manyToManyRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickManyToManyObjectRelationIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setManyToManyObjectRelation([$this->oldRelation]);
        $this->testObject->save();
        $objectbrick->setManyToManyObjectRelation([$this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.manyToManyObjectRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }
}
