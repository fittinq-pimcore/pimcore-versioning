<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning;

use Exception;
use Fittinq\Pimcore\Versioning\Versioning\ChangedFieldsExtractor;
use Pimcore\Model\DataObject\Service;
use Pimcore\Model\DataObject\TestObject;

class Configuration
{
    public function configure(): ChangedFieldsExtractor
    {
        return new ChangedFieldsExtractor();
    }

    /**
     * @throws Exception
     */
    public function setUpTestObject(string $path): TestObject
    {
        $object = new TestObject();
        $object->setKey("TestObject" . uniqid());
        $object->setParent(Service::createFolderByPath($path));

        return $object;
    }
}
