<?php
declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\DataObject;

use Exception;
use Fittinq\Pimcore\Versioning\Versioning\ChangedFieldsExtractor;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\Data\BlockElement;
use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\QuantityValue\Unit;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class BlockTest extends TestCase
{
    private TestObject $testObject;
    private TestObject $oldRelation;
    private TestObject $newRelation;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = new ChangedFieldsExtractor();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/DataObject/Block');
        $this->oldRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->oldRelation->save();
        $this->newRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->newRelation->save();
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockInputIfFieldChanges()
    {
        $this->testObject->setBlock([["input" => new BlockElement('input', 'input', 'old')]]);
        $this->testObject->save();
        $this->testObject->setBlock([["input" => new BlockElement('input', 'input', 'new')]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockCheckboxIfFieldChanges()
    {
        $this->testObject->setBlock([["checkbox" => new BlockElement('checkbox', 'checkbox', false)]]);
        $this->testObject->save();
        $this->testObject->setBlock([["checkbox" => new BlockElement('checkbox', 'checkbox', true)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnBlockNumberIfFieldChanges()
    {
        $this->testObject->setBlock([["number" => new BlockElement('number', 'number', 1)]]);
        $this->testObject->save();
        $this->testObject->setBlock([["number" => new BlockElement('number', 'number', 2)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnBlockQuantityValueIfFieldValueChanges()
    {
        $this->testObject->setBlock(
            [["quantityValue" => new BlockElement('quantityValue', 'quantityValue', new QuantityValue(10))]]
        );
        $this->testObject->save();

        $this->testObject->setBlock(
            [["quantityValue" => new BlockElement('quantityValue', 'quantityValue', new QuantityValue(20))]]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }


    /**
     * @throws Exception
     */
    public function test_returnBlockQuantityValueIfFieldUnitChanges()
    {
        $this->testObject->setBlock(
            [
                [
                    "quantityValue" => new BlockElement(
                        'quantityValue',
                        'quantityValue',
                        new QuantityValue(10, Unit::getByAbbreviation('g'))
                    )
                ]
            ]
        );
        $this->testObject->save();
        $this->testObject->setBlock(
            [
                [
                    "quantityValue" => new BlockElement(
                        'quantityValue',
                        'quantityValue',
                        new QuantityValue(10, Unit::getByAbbreviation('kg'))
                    )
                ]
            ]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockSelectIfFieldChanges()
    {
        $this->testObject->setBlock([["select" => new BlockElement('select', 'select', 'old')]]);
        $this->testObject->save();

        $this->testObject->setBlock([["select" => new BlockElement('select', 'select', 'new')]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockMultiSelectIfFieldChanges()
    {
        $this->testObject->setBlock([["multiSelect" => new BlockElement('multiSelect', 'multiselection', ['old'])]]);
        $this->testObject->save();

        $this->testObject->setBlock([["multiSelect" => new BlockElement('multiSelect', 'multiselection', ['new'])]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockManyToOneRelationIfFieldChanges()
    {
        $this->testObject->setBlock(
            [["manyToOneRelation" => new BlockElement('manyToOneRelation', 'manyToOneRelation', $this->oldRelation)]]
        );
        $this->testObject->save();
        $this->testObject->setBlock(
            [["manyToOneRelation" => new BlockElement('manyToOneRelation', 'manyToOneRelation', $this->newRelation)]]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockManyToManyRelationIfFieldChanges()
    {
        $this->testObject->setBlock(
            [
                [
                    "manyToManyRelation" => new BlockElement(
                        'manyToManyRelation',
                        'manyToManyRelation',
                        [$this->oldRelation]
                    )
                ]
            ]
        );
        $this->testObject->save();
        $this->testObject->setBlock(
            [
                [
                    "manyToManyRelation" => new BlockElement(
                        'manyToManyRelation',
                        'manyToManyRelation',
                        [$this->newRelation]
                    )
                ]
            ]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockManyToManyObjectRelationIfFieldChanges()
    {
        $this->testObject->setBlock(
            [
                [
                    "manyToManyObjectRelation" => new BlockElement(
                        'manyToManyObjectRelation',
                        'manyToManyObjectRelation',
                        [$this->oldRelation]
                    )
                ]
            ]
        );
        $this->testObject->save();
        $this->testObject->setBlock(
            [
                [
                    "manyToManyObjectRelation" => new BlockElement(
                        'manyToManyObjectRelation',
                        'manyToManyObjectRelation',
                        [$this->newRelation]
                    )
                ]
            ]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnBlockIfBlockAmountChanges()
    {
        $this->testObject->setBlock([["input" => new BlockElement('input', 'input', 'old')]]);
        $this->testObject->save();
        $this->testObject->setBlock(
            [
                ["input" => new BlockElement('input', 'input', 'old')],
                ["input" => new BlockElement('input', 'input', 'new')]
            ]
        );
        $this->testObject->save();

        $this->assertEquals(['TestObject.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }
}
