<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\DataObject;

use Exception;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class DataObjectTest extends TestCase
{
    private TestObject $testObject;
    private TestObject $oldRelation;
    private TestObject $newRelation;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/DataObject/DataObject');
        $this->oldRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->oldRelation->save();
        $this->newRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->newRelation->save();
    }

    /**
     * @throws Exception
     */
    public function test_returnInputIfFieldChanges()
    {
        $this->testObject->setInput('old');
        $this->testObject->save();
        $this->testObject->setInput('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.input'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTextAreaIfFieldChanges()
    {
        $this->testObject->setTextArea('old');
        $this->testObject->save();
        $this->testObject->setTextArea('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.textArea'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnCheckboxIfFieldChanges()
    {
        $this->testObject->setCheckbox(true);
        $this->testObject->save();
        $this->testObject->setCheckbox(false);
        $this->testObject->save();

        $this->assertEquals(['TestObject.checkbox'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnQuantityValueIfFieldChanges()
    {
        $quantityValue = new QuantityValue(10);
        $this->testObject->setQuantityValue($quantityValue);
        $this->testObject->save();

        $quantityValue->setValue(20);
        $this->testObject->setQuantityValue($quantityValue);
        $this->testObject->save();

        $this->assertEquals(['TestObject.quantityValue'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnQuantityValueIfFieldChangesFromNull()
    {
        $quantityValue = new QuantityValue(10);
        $this->testObject->setQuantityValue(null);
        $this->testObject->save();

        $quantityValue->setValue(20);
        $this->testObject->setQuantityValue($quantityValue);
        $this->testObject->save();

        $this->assertEquals(['TestObject.quantityValue'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnSelectIfFieldChanges()
    {
        $this->testObject->setSelect('old');
        $this->testObject->save();
        $this->testObject->setSelect('new');
        $this->testObject->save();

        $this->assertEquals(['TestObject.select'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnMultiselectIfFieldChanges()
    {
        $this->testObject->setMultiselect(['old']);
        $this->testObject->save();
        $this->testObject->setMultiselect(['new']);
        $this->testObject->save();

        $this->assertEquals(['TestObject.multiSelect'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToOneRelationIfFieldChanges()
    {
        $this->testObject->setManyToOneRelation($this->oldRelation);
        $this->testObject->save();
        $this->testObject->setManyToOneRelation($this->newRelation);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToOneRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToOneRelationIfFieldChangesFromNull()
    {
        $this->testObject->setManyToOneRelation(null);
        $this->testObject->save();
        $this->testObject->setManyToOneRelation($this->newRelation);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToOneRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToManyRelationIfFieldChanges()
    {
        $this->testObject->setManyToManyRelation([$this->oldRelation]);
        $this->testObject->save();
        $this->testObject->setManyToManyRelation([$this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToManyRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToManyObjectRelationIfFieldChanges()
    {
        $this->testObject->setManyToManyObjectRelation([$this->oldRelation]);
        $this->testObject->save();

        $this->testObject->setManyToManyObjectRelation([$this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToManyObjectRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToManyObjectRelationIfFieldChangesFromNull()
    {
        $this->testObject->setManyToManyObjectRelation(null);
        $this->testObject->save();

        $this->testObject->setManyToManyObjectRelation([$this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToManyObjectRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnManyToManyObjectRelationIfFieldAmountChanges()
    {
        $this->testObject->setManyToManyObjectRelation([$this->oldRelation]);
        $this->testObject->save();

        $this->testObject->setManyToManyObjectRelation([$this->oldRelation, $this->newRelation]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.manyToManyObjectRelation'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }
}
