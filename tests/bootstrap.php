<?php

declare(strict_types=1);

use App\Kernel;
use Pimcore\Bootstrap;

include "vendor/autoload.php";

Bootstrap::setProjectRoot();
Bootstrap::bootstrap();

$kernel = new Kernel('dev', true);
$kernel->boot();

Pimcore::setKernel($kernel);

