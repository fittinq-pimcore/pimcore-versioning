<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning;

use Exception;
use Fittinq\Pimcore\Versioning\Exceptions\NotEnoughVersionsException;
use Pimcore\Model\DataObject\Concrete;

class ChangedFieldsExtractor
{
    private FieldTypeExtractorFactory $fieldTypeExtractorFactory;

    public function __construct()
    {
        $this->fieldTypeExtractorFactory = new FieldTypeExtractorFactory();
    }

    /**
     * @throws NotEnoughVersionsException
     * @throws Exception
     */
    public function getLatestChanges(Concrete $concrete): array
    {
        /* note that this  set versions line is here, so we force the update of versions. This should never cause any
           problems, but may seem weird.
         */
        $concrete->setVersions($concrete->getDao()->getVersions());
        $versions = $concrete->getVersions();

        if (count($versions) < 2) {
            throw new NotEnoughVersionsException();
        }

        $currentVersion = array_pop($versions)->loadData();
        $previousVersion = array_pop($versions)->loadData();

        return $this->compareVersions($currentVersion, $previousVersion);
    }

    /**
     * @throws Exception
     */
    private function compareVersions(Concrete $lhs, Concrete $rhs): array
    {
        $changedFields = [];
        $classDefinitions = $lhs->getClass()->getFieldDefinitions();

        foreach ($classDefinitions as $classDefinition) {
            $extractor = $this->fieldTypeExtractorFactory->getFieldTypeExtractor($classDefinition->getFieldtype());

            $changedFields = array_merge(
                $changedFields,
                $extractor->getChangedFields(
                    $classDefinition,
                    $lhs,
                    $rhs
                )
            );
        }

        return array_map(
            function ($item) use ($lhs) {
              return $lhs->getClass()->getName() . '.' .$item;
            },
            $changedFields
        );
    }
}
