<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning;

class FieldType
{
    const MANY_TO_ONE_RELATION = 'manyToOneRelation';
    const MANY_TO_MANY_RELATION = 'manyToManyRelation';
    const MANY_TO_MANY_OBJECT_RELATION = 'manyToManyObjectRelation';
    const INPUT = 'input';
    const NUMBER = 'number';
    const TEXTAREA = 'textarea';
    const QUANTITYVALUE = 'quantityValue';
    const OBJECTBRICKS = 'objectbricks';
    const LOCALIZEDFIELDS = 'localizedfields';
    const CHECKBOX = 'checkbox';
    const SELECT = 'select';
    const MULTISELECTION = 'multiselect';
    const NEWSLETTER_ACTIVE = 'newsletterActive';
    const NEWSLETTER_CONFIRMED = 'newsletterConfirmed';
    const BLOCK = 'block';
}
