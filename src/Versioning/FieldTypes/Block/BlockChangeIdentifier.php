<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Block;

use Pimcore\Model\DataObject\Data\BlockElement;

class BlockChangeIdentifier
{
    /**
     * @param BlockElement[][] $lhs
     * @param BlockElement[][] $rhs
     */
    public function hasChanged(array $lhs, array $rhs): bool
    {
        /*
         * When the number of blocks differs, it means we can assume all fields to be changed. For this we simply return
         * the whole field name as the key. This can be used as a catch-all.
         */
        if (count($lhs) !== count($rhs)) {
            return true;
        }

        return $this->compareBlockElements($lhs, $rhs);
    }

    /**
     * @param BlockElement[][] $lhs
     * @param BlockElement[][] $rhs
     */
    public function compareBlockElements(array $lhs, array $rhs): bool
    {
        for ($i = 0; $i < count($lhs); $i++) {
            foreach ($lhs[$i] as $key => $lhsValue) {
                $rhsValue = $rhs[$i][$key];

                if (serialize($lhsValue->getData()) !== serialize($rhsValue->getData())) {
                    // And lastly compare the actual data.
                    return true;
                }
            }
        }

        return false;
    }
}
