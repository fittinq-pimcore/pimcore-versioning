<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Block;

use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;

class Block implements FieldTypeExtractor
{
    private BlockChangeIdentifier $blockChangeIdentifier;

    public function __construct()
    {
        $this->blockChangeIdentifier = new BlockChangeIdentifier();
    }

    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        $fieldName = $classDefinition->getName();

        $lhsValue = $lhs->getValueForFieldName($fieldName);
        $rhsValue = $rhs->getValueForFieldName($fieldName);

        if ($this->blockChangeIdentifier->hasChanged($lhsValue, $rhsValue)) {
            return [$fieldName];
        }

        return [];
    }
}
