<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Localizedfields;

use Fittinq\Pimcore\Localization\Locale\LocaleRepository;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Localizedfield;

class Localizedfields implements FieldTypeExtractor
{
    /**
     * @var string[]
     */
    private array $changedFields;

    private LocaleRepository $localeRepository;

    public function __construct()
    {
        $this->localeRepository = new LocaleRepository();
    }

    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        /** @var Data\Localizedfields $classDefinition */
        $this->changedFields = [];

        $fieldName = $classDefinition->getName();
        $lhsLocalizedField = $lhs->getValueForFieldName($fieldName);
        $rhsLocalizedField = $rhs->getValueForFieldName($fieldName);

        $this->getChangedFieldsChildren($classDefinition->getFieldDefinitions(), $lhsLocalizedField, $rhsLocalizedField);

        return array_values(array_unique($this->changedFields));
    }

    private function getChangedFieldsChildren(array $definitions, Localizedfield $lhsLocalizedField, Localizedfield $rhsLocalizedField): void
    {
        foreach ($definitions as $definition) {
                foreach ($this->localeRepository->getLocales() as $locale) {
                $this->getChangedFieldsData($definition->getName(), $locale->getLocale(), $lhsLocalizedField, $rhsLocalizedField);
            }
        }
    }

    private function getChangedFieldsData(string $name, string $locale, Localizedfield $lhsLocalizedField, Localizedfield $rhsLocalizedField): void
    {
        $lhsValue = $lhsLocalizedField->getLocalizedValue($name, $locale);
        $rhsValue = $rhsLocalizedField->getLocalizedValue($name, $locale);

        if ($lhsValue !== $rhsValue) {
            $this->changedFields[] = "{$name}.{$locale}";
            $this->changedFields[] = $name;
        }
    }
}
