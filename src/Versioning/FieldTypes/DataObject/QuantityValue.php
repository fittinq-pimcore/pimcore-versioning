<?php
declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject;

use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;

class QuantityValue implements FieldTypeExtractor
{
    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        $fieldName = $classDefinition->getName();

        $lhsValue = $lhs->getValueForFieldName($fieldName);
        $rhsValue = $rhs->getValueForFieldName($fieldName);

        if (!$lhsValue && !$rhsValue) {
            // They are both falsy, so we consider them equal.
            return [];
        }

        if (!$lhsValue || !$rhsValue) {
            // Because of the previous test we know one is false, so if one of them is true here, something changed.
            return [$fieldName];
        }

        return $lhsValue->getValue() !== $rhsValue->getValue() || $lhsValue->getUnit()->getAbbreviation(
        ) !== $rhsValue->getUnit()->getAbbreviation() ? [$fieldName] : [];
    }
}
